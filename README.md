## Welcome to Broccolizer

This application helps you keep track of your diet

## System requirements

You will need to install locally:

- [Docker](https://www.docker.com/)
- [docker-compose](https://docs.docker.com/compose/)
- [yarn](https://yarnpkg.com/). Make sure Yarn is at its latest version.

## Development

Clone the repository:

```bash
$ git clone git@gitlab.com:nyan_dev/broccolizer-v2.git
```

Start application:

```bash
$ docker-compose up
```

Run database migration:

```bash
$ docker-compose run web rake db:migrate
```

## Tutorial

Este es el proyecto vehicular utilizado en el taller de "Fullstacking en 2h".

Accede a los apuntes:
- [Primera sesión](docs/TUTORIAL_PRIMERA_SESION.md)
- [Segunda sesión](docs/TUTORIAL_SEGUNDA_SESION.md)
